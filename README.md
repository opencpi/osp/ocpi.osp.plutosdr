This is the source distribution of the OpenCPI PLUTO System Support Project (OSP).

OpenCPI is Open Source Software, licensed with the LGPL3. See LICENSE.txt. 

Getting Started:
---

See Pluto_Getting_Started_Guide on opencpi.gitlab.io under the PlutoSDR OSP Documentation section.
