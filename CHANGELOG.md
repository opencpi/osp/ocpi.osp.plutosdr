# [v2.4.7](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.6...v2.4.7) (2024-01-09)

Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

### Bug Fixes
- **devops**: prevent commits from automatically launching a CI/CD pipeline. (!47)(524b2a57)

### Enhancements
- **doc**: update GSG doc to new best practice. (!41)(28449a86)
- **hdl base**: consolidate constraints in a single parameterized file, ensure all assemblies build, remove Makefiles. (!41)(28449a86)

### Miscellaneous
- **osp**: fix `spec=` attributes in XML files. (!41)(28449a86)

# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.3...v2.4.4) (2023-01-22)

No Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

# [v2.4.3](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.2...v2.4.3) (2022-10-11)

No Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.1...v2.4.2) (2022-05-26)

No Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

# [v2.4.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.4.0...v2.4.1) (2022-03-17)

Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

### Enhancements
- **app**: use newer features to simplify the fsk app. (!40)(6b9d3d8c)

# [v2.4.0](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.3.4...v2.4.0) (2022-01-25)

No Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

# [v2.3.4](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.3.3...v2.3.4) (2021-12-17)

No Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)

# [v2.3.3](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.3.2...v2.3.3) (2021-11-30)

No Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

# [v2.3.2](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.3.1...v2.3.2) (2021-11-08)

No Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

# [v2.3.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.3.0...v2.3.1) (2021-10-13)

No Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

# [v2.3.0](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

No Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.2.1...v2.3.0-rc.1) (2021-08-26)

No Changes/additions since [OpenCPI Release v2.2.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.1)

# [v2.2.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.2.0...v2.2.1) (2021-07-22)

No Changes/additions since [OpenCPI Release v2.2.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.0)

# [v2.2.0](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.1.1...v2.2.0) (2021-07-08)

Changes/additions since [OpenCPI Release v2.1.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.1)

### Enhancements
- **app,comp**: remove extra/unsupported apps and workers. (!39)(484e6222)

# [v2.1.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.1.0...v2.1.1) (2021-05-20)

No Changes/additions since [OpenCPI Release v2.1.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0)

# [v2.1.0](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.1.0-rc.2...v2.1.0) (2021-03-18)

Changes/additions since [OpenCPI Release v2.1.0-rc.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0-rc.2)

### Enhancements
- **comp,hdl base**: update plutosdr platform worker to drive usingPPS. (!37)(f817f23c)

### Miscellaneous
- **app,runtime**: add timestamping drc for e31x platform. (!38)(b36b42de)

# [v2.1.0-rc.2](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.1.0-rc.1...v2.1.0-rc.2) (2021-03-04)

Changes/additions since [OpenCPI Release v2.1.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0-rc.1)

### Enhancements
- **runtime**: enable drc stop_config. (!34)(d21977d6)

### Bug Fixes
- **comp**: fix bug in drc worker implementation. (!35)(7f77ed40)

# [v2.1.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.1.0-beta.1...v2.1.0-rc.1) (2021-02-08)

Changes/additions since [OpenCPI Release v2.1.0-beta.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.1.0-beta.1)

### Enhancements
- **hdl base**: update plutosdr_data_src_qadc_ad9361_sub_data_sink_qdac_ad9361_sub_mode_2_cmos.xdc to properly constrain things for the ADC one clock per sample. (!27)(fac80249)
- **hdl base**: increate sdp width to 64 bits on pluto. (!29)(16896d7c)

# [v2.1.0-beta.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.0.1...v2.1.0-beta.1) (2020-12-17)

Changes/additions since [OpenCPI Release v2.0.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.1)

### Enhancements
- **osp**: update fsk app to use new drc with inferred slaves. (!26)(f610aea3)

### Bug Fixes
- **doc**: Update accuracy of information in Plutosdr_Getting_Sarted_Guide.tx. (!9)(bab0be44)

# [v2.0.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.0.0...v2.0.1) (2020-11-18)

No Changes/additions since [OpenCPI Release v2.0.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0)

# [v2.0.0](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.0.0-rc.2...v2.0.0) (2020-10-06)

No changes/additions since [OpenCPI Release v2.0.0-rc.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0-rc.2)

# [v2.0.0-rc.2](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.0.0-rc.1...v2.0.0-rc.2) (2020-09-22)

Changes/additions since [OpenCPI Release v2.0.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.0.0-rc.1)

### New Features
- **osp**: add DRC support to plutosdr. (!16)(1ab1930f)
- **osp**: update PlutoSDR rcc platform to adi_plutosdr0_32. (!17)(d48d4a56)

### Enhancements
- **osp**: add ADC/DAC split clock support to plutosdr. (!16)(1ab1930f)

# [v2.0.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v2.0.0-beta.1...v2.0.0-rc.1) (2020-08-29)

Changes/additions since OpenCPI Release v2.0.0-beta.1

### Bug Fixes
- **tools**: fix nfs kernel config patch. (!8)(b925c4e8)

# [v2.0.0-beta.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/v1.7.0...v2.0.0-beta.1) (2020-07-28)

Changes/additions since OpenCPI Release v1.7.0

### Enhancements
- **devops**: remove testing on CentOS 6. (!11)(2869f8b9)

# [v1.7.0](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/d111c1ae...v1.7.0) (2020-07-09)

No change/additions since [PlutoSDR Release v1.7.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v1.7.0-rc.1)

# [v1.7.0-rc.1](https://gitlab.com/opencpi/osp/ocpi.osp.plutosdr/-/compare/d111c1ae...v1.7.0-rc.1) (2020-07-06)

Initial release of PlutoSDR for OpenCPI

### New Features
- **doc**: PlutoSDR Getting Started Guide. (!3)(e9d83d15)
- **osp**: initial pluto support. (!2)(f8b1ac67)
- **osp**: remove fir real sse filter from repository. (!4)(abad953c)

### Enhancements
- **devops**: add plutosdr CI pipeline. (!6)(1b5ec524)

### Miscellaneous
- **doc**: Remove utilization.inc from repo. (!5)(7bc68e8d)
