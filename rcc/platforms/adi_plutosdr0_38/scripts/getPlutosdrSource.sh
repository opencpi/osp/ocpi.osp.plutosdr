#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.
set -e
mkdir gen/kernel-artifacts
if test "$1" = "" -o "$1" = "--help" -o "$1" = "-help"; then
  cat <<EOF
This script establishes a local clone of Analog Devices pluto sdr firmware source tree. 
This script has one argument, which is the directory in which the git clone will be created.
Usually it is "git".
EOF
  exit 1
fi
git_dir=$1
set -e
if test ! -L $git_dir; then
  mkdir -p $git_dir
fi

cd $git_dir
if test -d plutosdr-fw; then
  echo The $git_dir/plutosdr-fw directory already exists. It will be removed.
  rm -r -f plutosdr-fw 
fi

echo Cloning/downloading the pluto sdr firmware v0.38 from github.
git clone https://github.com/analogdevicesinc/plutosdr-fw.git 

cd plutosdr-fw
echo Using v0.38 Branch 
git checkout -f v0.38
