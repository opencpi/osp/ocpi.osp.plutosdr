#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

#export TOOLCHAIN=/home/aaron/opencpi-develop/projects/osps/ocpi.osp.plutosdr/rcc/platforms/adi_plutosdr0_38/gen/toolchain/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf/bin/

echo Building Linux kernel
cd gen/kernel-artifacts/plutosdr-fw
git submodule update --init -- buildroot
git submodule update --init -- u-boot-xlnx
git submodule update --init -- linux

export CROSS_COMPILE=arm-linux-gnueabihf-

# add make prepare
# add make modules
git apply ../../../scripts/plutosdr-fw-make-modules.patch

make build/zImage

echo Preparing the kernel-headers tree based on the built kernel.
cd ..
rm -r -f kernel-headers
mkdir kernel-headers
RELDIR=`pwd`
# copy that avoids errors when caseinsensitive file systems are used (MacOS...)
#cp -R pluto-fw/linux-xlnx/{Makefile,Module.symvers,include,scripts,arch} kernel-headers
(cd ../kernel-artifacts/plutosdr-fw/linux;
  for f in Makefile Module.symvers include scripts arch/arm .config; do
    find $f -type d -exec mkdir -p $RELDIR/kernel-headers/{} \;
    find $f -type f -exec sh -c \
     "if test -e $RELDIR/kernel-headers/{}; then
        echo File {} has a case sensitive duplicate which will be overwritten.
        rm -f $RELDIR/kernel-headers/{}
      fi
      cp {} $RELDIR/kernel-headers/{}" \;
  done
  for i in $(find -name 'Kconfig*'); do
    mkdir -p $RELDIR/kernel-headers/$(dirname $i)
    cp $i $RELDIR/kernel-headers/$(dirname $i)
  done
)
rm -r -f kernel-headers/arch/arm/boot
find kernel-headers -name "*.[csSo]" -exec rm {} \;
rm kernel-headers/scripts/{basic,mod}/.gitignore
echo ============================================================================================
echo The kernel-headers directory/package has been created in $(basename $RELDIR)
echo It is now ready for building the OpenCPI linux kernel driver for plutosdr
echo ============================================================================================
echo Removing *.cmd
find kernel-headers -name '*.cmd' -delete
echo Removing x86_64 binaries
find kernel-headers/scripts | xargs file | grep "ELF 64-bit" | cut -f1 -d: | xargs -tr -n1 rm
echo Removing auto.conf
rm kernel-headers/include/config/auto.conf
echo Restoring source to removed binaries
(cd ../kernel-artifacts/plutosdr-fw/linux;
  for f in $(find scripts/ -name '*.c' -o -name 'zconf.tab'); do
    mkdir -p $RELDIR/kernel-headers/$(dirname $f)
    cp $f $RELDIR/kernel-headers/$(dirname $f)
  done
  mkdir -p $RELDIR/kernel-headers/tools/include
  cp -R tools/include/tools $RELDIR/kernel-headers/tools/include
)
echo Removing unused large headers
rm -rf kernel-headers/include/linux/{mfd,platform_data}
tar cvzf kernel-headers.tar kernel-headers/
