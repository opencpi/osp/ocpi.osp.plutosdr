# SET THIS VARIABLE to the part (die-speed-package, e.g. xc7z020-1-clg484) for the platform!
HdlPart_plutosdr=xc7z010-1-clg225
# Set this variable to the names of any other component libraries with devices defined in this
# platform. Do not use slashes.  If there is an hdl/devices library in this project, it will be
# searched automatically, as will "devices" in any projects this project depends on.
# An example might be something like "our_special_devices", which would exist in this or
# other projects.
# ComponentLibraries_plutosdr=
HdlRccPlatform_plutosdr=adi_plutosdr0_38

# The subset of my project dependencies that users of this platform should reference.
# Since this may be used in the context of another project that may NOT have these
# dependencies, they will only be used *last* for search purposes.
# This means that any device dependencies of this platform should be fully qualified
HdlPlatformProjectDependencies_plutosdr=ocpi.platform
