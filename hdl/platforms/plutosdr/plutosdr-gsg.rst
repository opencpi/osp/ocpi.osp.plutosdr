.. %%NAME-CODE%% Getting Started Guide Documentation


.. _%%NAME-CODE%%-gsg:

.. This is a template for creating an OpenCPI Getting Started Guide
   for an OpenCPI system. Copy this template file, rename it, and
   edit the contents to your requirements.

.. Below are definitions for copyright and trademark symbols.

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:

.. Below are default substitution strings used in this template
   in headings and text as placeholders for the given system
   name, its vendor, and the product family it belongs to,
   if any. Use any or all of these strings "as is" or customize
   them to your requirements.

.. Details on how to use substitution strings are given in the
   section "Using Include Files and Substitution Strings to
   Share Common Information" in the OpenCPI User Guide.
   
.. |system_name| replace:: PlutoSDR

.. The |system_name| definition above specifies a substitution
   string for the name for the system to be used when referring
   to the system in section headings and text. Replace "MySystemName"
   with the common/shorthand name used for the given system in the
   product vendor's documentation. Examples: "ZedBoard", "ZCU102"
   "E310", "PlutoSDR".

.. |vendor_name| replace:: Analog Devices
			   
.. The |vendor_name| definition above specifies a substitution
   string for the name for the system's vendor to be used in
   section headings and text. Replace "MySystemVendor" with
   the system vendor's name. Examples: "Digilent", "Xilinx"
   "Ettus Research", "Analog Devices".

.. |product_family| replace:: ADALM-PLUTO

.. The |product_family| definition above defines a substitution string
   for the name of the product family/ product category/series to which
   the system belongs (if any) to be used when introducing the system
   in the Overview section and in other locations as necessary.
   Replace "MySystemProductFamily" with the system's product family
   name. Examples: "Zynq-7000", "Zynq", "Universal Software Radio Peripheral (USRP)",
   "ADALM-PLUTO".

.. |device_family| replace:: Advanced Active Learning Module

.. The |device_family| definition above defines a substitution string for
   the name for the device/board/system family (if any) within the product
   family to which the system belongs to be used when introducing the system
   in the Overview section and in other locations as necessary. Replace
   "MyProductDeviceFamily" with the system's product device family name.
   Examples: "All Programmable (AP)", "UltraScale+", "E3xx Series",
   "Advanced Active Learning Module".

.. Note that vendors frequently change the names of products, product families,
   series, etc. When describing the given system, use the naming convention
   that appears in the vendor product brief that corresponds to the system
   you're describing.

.. New strings for this GSG

.. |rcc_platform_name| replace:: ``adi_plutosdr0_38``

.. |hdl_platform_name| replace:: ``plutosdr``

OpenCPI |vendor_name| |system_name| Getting Started Guide
=========================================================

.. This is the main RST file for a getting started guide (GSG) for
   an OpenCPI system. An OpenCPI system consists of platforms.
   The platforms used in a given system can be platforms that are
   re-usable in other systems or they can be platforms that are used
   only in the given system.

.. Reusable platforms generally have their own Getting Started Guides to
   describe their installation and setup. Examples of reusable platforms
   (using their OpenCPI platform names) are the xilinx24_1_aarch32 and
   xilinx24_1_aarch64 RCC platforms (Xilinx Linux 2024.1 32-bit and Xilinx
   Linux 2024.1 64-bit RCC platforms) and the picoevb PCI Express-based HDL
   platform (RHS Research PicoEVB, described in the PicoEVB Getting Started Guide).
   For reusable platforms, the system GSG should refer to the platform's
   GSG and only describe those differences (if any) in how the platform
   is used in the given system.
   Note that the reusable RCC platforms do not currently have their own GSGs.
   Installation information about these RCC platforms is provided in the
   OpenCPI Installation Guide.

.. Platforms that are truly specific to a given system should be documented
   in a separate section in this system GSG. The section should follow the
   outline/organization used in the OpenCPI platform getting started guide template.
   Examples are the adi_plutosdr0_38 RCC platform and the plutosdr HDL platform
   (specific to the ADALM-PLUTO/PlutoSDR system and documented in the PlutoSDR
   Getting Started Guide), the zed HDL platform (specific to the ZedBoard system
   and described in the Zedboard Getting Started Guide), the e31x HDL platform
   (specific to the E31X system and described in the E31x Getting Started Guide),
   and the adrv9361 HDL platform (specific to the ADRV9361-Z7035 system and described
   in the ADRV9361-Z7035 Getting Started Guide).

.. A system GSG should also describe any setup issues that are separate from
   the underlying platforms.
   
.. Each system should have its own GSG. The main RST file for a GSG should
   be located in the systems/<system-name>/ directory in the project (usually
   an OSP) along with the other assets specific to the given system.
   Images used in the system GSG should also be located in this directory
   unless you are using "include" files as described below.

Document Revision History
-------------------------

.. In the table below, supply the document's revision number,
   a brief description of the update, and the date at which the
   update was made. The revision number can be any sequential
   numbering scheme or it can be the same as the
   OpenCPI version in which the document is released.

.. csv-table:: OpenCPI |vendor_name| |system_name| Getting Started Guide: Revision History
   :header: "Revision", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v1.7", "Initial release", "4/2020"
   "v2.0", "Updates made to installation process", "10/2020"
   "v2.4", "Conversion to Sphinx/RST, update to GSG template", "12/2022"

How to Use This Document
------------------------
  
.. To start this section, give a 1-sentence description of this OpenCPI system.
   Use the proper name of the system and apply the appropriate
   trademarks.
   
This document provides information that is specific to setting up the
OpenCPI |vendor_name| |product_family| (|system_name|)
for use with OpenCPI. It describes system-specific details about setting up
the system as a whole.

Use this document in conjunction with the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.
The following OpenCPI documents can also be used as references for the information in this document:

* `OpenCPI User Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf>`_
  
* `OpenCPI Glossary <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Glossary.pdf>`_

Note that the *OpenCPI Glossary* is also contained in both the *OpenCPI Installation Guide* and the
*OpenCPI User Guide*.

This document assumes a basic understanding of the Linux command line (or "shell") environment.

Overview
--------

.. Provide a brief overview of the system that describes how it can be used for OpenCPI.
   Features that are irrelevant to its use as an OpenCPI system do not need to be mentioned.
   Provide a link to the vendor product brief and give the OpenCPI name for the system
   (e.g., e31x, zed, zcu102, microzed,...)

The |vendor_name| |product_family| (|system_name|) is a software-defined radio (SDR) |device_family|.
See the product brief at https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/adalm-pluto.html#eb-overview
for detailed information about the |system_name|.

.. Next, identify the platforms used in the system and how they are interconnected (fabric, bus, ...).

The |system_name| is a Xilinx\ |reg| Zynq-based system
that consists of an RCC/CPU (software) "PS" platform and an HDL (FPGA) "PL" platform
connected by an Advanced Extensible Interface (AXI) bus.
In OpenCPI, the |system_name| RCC platform is called |rcc_platform_name|
and the |system_name| HDL platform is called |hdl_platform_name|.

Installation Summary
--------------------
To set up the |system_name| system for OpenCPI development, perform the following steps:

* Select and configure the mode of operation for the |system_name|.  See :ref:`mode-intro`.

* Update the |system_name| firmware. See :ref:`update-firmware`.

* Perform the platform installation steps described in the section "Installation Steps for Platforms"
  in the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
  to install and build the OpenCPI |rcc_platform_name| and |hdl_platform_name| platforms.

* Perform the post-platform installation steps described in the section "Installation Steps for Systems After Their Platforms Are Installed"
  in the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_, supplying the system-specific details in :ref:`setup-system`.

.. _mode-intro:

Enabling |system_name| Modes of Operation
--------------------------------------------
The |system_name| can operate in two different modes:

* |system_name|-as-Device - in this mode, the |system_name| is a USB device to a development host.

* |system_name|-as-Host - in this mode, the |system_name| is the USB host. No development host is required. Supported USB devices include mass storage devices, Ethernet, and WiFi devices.

To make the |system_name| act the most similar to other supported devices,
use the |system_name|-as-Host mode with a network adapter.

.. _device-mode:

Enabling |system_name|-as-Device Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To enable this mode, connect a cable between the development host
and the micro-USB port  located at the center of the |system_name|
and labeled with the "USB port" symbol, as shown in :numref:`pluto-usb-diagram`

.. _pluto-usb-diagram:

.. figure:: pluto_usb.jpg
   :alt: 
   :align: center

   Connected micro-USB Cable

Both power and data are provided through this connection.

When the |system_name| is in this mode, it emulates several devices that
appear to the development host as USB-attached devices:

* USB serial, which appears as a Linux ``tty`` device in ``/dev/ttyACMx`` for use by terminal emulators like ``screen``.
  
* Mass storage, which appears as a writable disk that can be used to update the non-volatile boot
  firmware inside the |system_name|.

* Remote Network Driver Interface Spec (RNDIS) Ethernet, which appears as an Ethernet link ``ethx``.
  The |system_name| IP address is set statically to ``192.168.2.1``. The development host will
  need to statically set its IP address on ``ethx`` to be on the same subnet as the |system_name|;
  for example, ``192.168.2.10``. If multiple |system_name| systems are required to be on the same
  development host, the |system_name| ``config.txt`` file must be updated to provide a unique
  Ethernet address for the |system_name|. The ``config.txt`` file is available as part of the
  emulated mass storage device when connected via USB on a development host. This step is necessary
  because by default the |system_name| systems have the same IP address from the factory. To resolve this problem,
  change the following lines in the ``config.txt`` file and modify the Ethernet address to be unique:
  ::

     # [NETWORK]
     # hostname = plutosdr
     # ipaddr = <unique IP address>
     # ipaddr_host = 192.168.2.10
     # netmask = 255.255.255.0

.. _host-mode:

Enabling |system_name|-as-Host Mode
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To enable this mode, you need the following items:

* USB On-the-Go (OTG) cable

* Compatible USB dongle

* USB power supply

When an Ethernet dongle is attached, the system’s IP address is issued by a DHCP server.
Alternatively, the system’s IP address can be statically set using the ``config.txt`` file
provided on the |system_name| emulated mass storage device.

:numref:`pluto-eth-diagram` shows the |system_name|-as-Host mode hardware configuration.

.. _pluto-eth-diagram:

.. figure:: pluto_ether.jpg
   :alt: 
   :align: center

   Connected OTG Ethernet Device

In this configuration, use SSH to access the |system_name|’s command prompt. Power must be applied
to the micro-USB port labeled with the power supply symbol.

For more information about this mode, see https://wiki.analog.com/university/tools/pluto/usb_otg_host_function_support.

.. _update-firmware:

Updating the |system_name| Firmware
-----------------------------------

.. note::
   
   The |system_name| must be in |system_name|-as-Device mode to perform a firmware update.

The |system_name| firmware must be updated in order to use the OpenCPI runtime environment.

The |system_name| uses an emulated mass storage device instead of an SD
card as a method to update its firmware. Use the steps below to update the device’s firmware.
Additional information can be found at https://wiki.analog.com/university/tools/pluto/users/firmware.

* Connect the |system_name| to the development host as described in :ref:`device-mode`.
  
* Download the prebuilt artifacts provided by |vendor_name| at
  https://github.com/analogdevicesinc/plutosdr-fw/releases/download/v0.38/plutosdr-fw-v0.38.zip.

* Move the file ``pluto.frm`` from the unzipped file downloaded in the previous step onto the device to update its firmware.

* Unmount the mass storage device.  If on a virtual machine, eject the USB device from the host as well.

The LEDs on the |system_name| should start to blink and continue blinking for about a minute.

When the firmware update is complete, the |system_name| reboots itself
and the emulated devices reappear on the development host.

Issues Regarding Storage
------------------------
The |rcc_platform_name| does not support NFS mounts. Also, the |system_name| only
has about 30 of volatile storage. To use a larger storage volume,
use a USB hub to connect a flash drive to the |system_name|.

.. _setup-system:

Setting up the |system_name| After its Platforms are Installed
--------------------------------------------------------------

.. In this section, describe any steps to enable the OpenCPI development
   and execution environment that need to be performed on the system as
   a whole (and NOT on the platforms in the system). Examples:

   - How to set up the system to boot from an SD card: location of
     relevant jumpers or switches (include images or link to product
     vendor documentation) and steps to configure them.

   - How to connect the system to an Ethernet network: location of
     relevant Ethernet ports, hardware items required for connection,
     process to connect.

   - Any system-specific details that are necessary for following the
     procedures described in the OpenCPI Installation Guide section
     "Installation Steps for Systems after their Platforms are Installed".
     Examples:

     - SD card setup details needed when following the procedures in the
       sections "Using SD Card Reader/Writer Devices", "Preparing the SD
       Card Contents", "Writing the SD Card", SD Card OpenCPI Startup Script Setup"
       for this system.

     - Serial console setup details needed when following the serial
       console-related procedures described in "Preparing the Development
       Host to Support Embedded Systems" and "Establishing a Serial Console
       Connection" for this system.

     - Details about setting up the modes of OpenCPI operation (server, network,
       or standalone) needed when following the procedures in the section
       "Configuring the Runtime Environment" for this system.

     - Details about running the standard installation test applications in
       the different modes of operation (server, network, standalone) needed
       when following the procedures in the section "Running the Test Application"
       for this system.

.. If possible, supply images for the locations of ports, switches, and jumpers
   and for switch and jumper settings. Otherwise, refer to the vendor hardware
   user manual.

.. The standard OpenCPI installation process described in "Installation Steps
   for Systems after their Platforms are Installed" must be used to enable
   OpenCPI for the given OpenCPI system unless there is a legitimate reason
   for providing something different.
   
.. Below is the introductory text for this section that is used in all system GSGs.

This section describes steps to enable the OpenCPI development and execution environment
that must be performed on the |system_name| after its platforms have been
installed. It also contains setup information specific to the |system_name| system
that is needed when performing the tasks described in the section
"Installation Steps for Systems After their Platforms are Installed"
in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.

.. Below is an example of how to provide a "details" section be used in a procedure
   described in "Installation Steps for Systems after their Platforms are Installed"
   in the OpenCPI Installation Guide.
  
Details for Establishing a Serial Console Connection for the |system_name|
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This section contains details about the |system_name| that pertain to the
instructions for setting up a serial I/O console described in the section
"Establishing a Serial Console Connection" in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_:

* On |system_name|, the serial console port operates at 115200 baud.

* The |system_name| must be in |system_name|-as-Device mode (see :ref:`device-mode`) to access the Linux console.

Details for Configuring the Runtime Environment for the |system_name| 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This section contains details about the |system_name| that pertain to the
instructions for setting up a serial I/O console described in the section
"Configuring the Runtime Environment" in the
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_:

* The primary mode of operation for the |system_name| is server mode.

* The |system_name| username and password is initially set to ``root:analog``.


